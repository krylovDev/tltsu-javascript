// const obj = {
// 	key1: 'value'
// }

// console.log(obj)
//
// const obj2 = obj
//
// obj2.key1 = 'new value'
// console.log(obj2)
// console.log(obj)

// ------------------------- The value can be a primitive data type -------------------------
// const obj = {
// 	key1: 'value',
// 	key2: 'value 2',
// 	asd: 55,
// 	true: true,
// 	null: null
// }
//
// console.log(obj.key1) // value
// console.log(obj['key2']) // value 2
// // console.log(obj['5']) // 55
// // console.log(obj.5) // Error
// console.log(obj.null)
//
// ------------------------- Writing a new value to an object -------------------------
// obj.newKey = 'new value'
// // console.log(obj)
//
// // delete obj.newKey
// // console.log(obj)
//
// delete obj.test
// console.log(obj)
// ------------------------- Checking for a key in the object -------------------------
// console.log('key2' in obj) // true
// console.log('key24' in obj) // false
//
// if ('key24' in obj) { // true
// 	console.log('1')
// } else { // false
// 	console.log('0')
// }


// ------------------------- The value can be a reference data type -------------------------
// const user = {
// 	name: 'Vladimir',
// 	email: 'any@gmail.com',
// 	logInfo: function() { // method
// 		console.log(`name: ${user.name}, email: ${user.email}`)
// 	},
// 	logInfo2() {
// 		console.log(`name: ${user.name}, email: ${user.email}`)
// 	}
// }

// function logInfo2() {
// 	console.log(`name: ${user.name}, email: ${user.email}`)
// }
//
// logInfo2()

// console.log(user.name)
// user.logInfo()
// user.logInfo2()



// const user = {
// 	name: 'Vladimir',
// 	email: 'any@gmail.com',
// 	logInfo() {
// 		console.log(`name: ${user.name}, email: ${user.email}`)
// 	},
// 	logInfo2() {
// 		console.log(`name: ${this.name}, email: ${this.email}`)
// 	},
// 	logThis() {
// 		console.log('this:', this)
// 	}
// }

// const secondUser = user
//
// secondUser.email = 'newAnyMail@gmail.com'
//
// console.log(secondUser.email) // 'newAnyMail@gmail.com'
// console.log(user.email) // 'newAnyMail@gmail.com'
// ------------------------- The 1st way to copy an object -------------------------
// Object assign
// const secondUser = Object.assign({},user)
// console.log(secondUser)
// secondUser.email = 'newAnyMail@gmail.com'
// console.log(secondUser.email) // 'newAnyMail@gmail.com'
// console.log(user.email) // 'any@gmail.com'

// user.logInfo()
// secondUser.logInfo()
// console.log(' ----------------- ')
// user.logInfo2()
// secondUser.logInfo2()

// user.logThis()
// secondUser.logThis()
// ------------------------- The 2nd way to copy an object -------------------------
// for in
// const newUser = {}
// console.log(newUser)
// for (let key in user) {
// 	newUser[key] = user[key]
// 	console.log(newUser)
// }
//
// console.log(newUser)





// ------------------------- ?. Optional chain -------------------------

// const user = {
// 	name: 'Vladimir',
// 	email: 'any@gmail.com',
// 	logInfo() {
// 		console.log(`name: ${this.name}, email: ${this.email}`)
// 	},
// 	parents: {
// 		mom: {
// 			phone: '1-23-44',
// 			email: 'mom@gmail.com'
// 		},
// 		dad: {
// 			phone: null,
// 			email: 'mom@gmail.com'
// 		},
// 	}
// }
//
// console.log(user.parents?.grandMom?.phone) // undefined (Falsy: 0 '' null undefined NaN)
// console.log(user.parents.dad.phone) // null
// console.log(user.parents.mom.phone) // 1-23-44
//
// if (user.parents?.grandMom?.phone) {
// 	console.log('1')
// } else {
// 	console.log('0')
// }

// ------------------------- What to do if you need to create many objects of the same type -------------------------
// const user = {
// 	name: 'Vladimir',
// 	email: 'any@gmail.com',
// }
//
// const user2 = {
// 	name: 'Vladimir2',
// 	email: 'any2@gmail.com',
// }


// 'Vladimir2','any2@gmail.com -> {}
// ------------------------- Object constructions -------------------------
function User(name, email) {
	this.name = name
	this.email = email
	this.logInfo = function() {
		console.log(`name: ${this.name}, email: ${this.email}`)
	}
	this.logInfo2 = () => {
		console.log(`name: ${this.name}, email: ${this.email}`)
	}
	this.logThis = function() {
		console.log('this', this)
	}
	this.logThis2 = () => {
		console.log('this', this)
	}
}
// new - creates a new instance.
const user = new User('Vladimir','any@gmail.com')
const user2 = new User('Vladimir2','any2@gmail.com')

// console.log(user)
// console.log(user2)

// user.logThis2()
// user2.logThis2()

const user3 = {
	name: 'Vladimir',
	email: 'any@gmail.com',
	phone: 12
}

const name = user3.name
console.log(name)
const {email,...otherProps1} = user3
console.log(email)
console.log(otherProps1) // {name: 'Vladimir', phone: 12}
console.log(user3)

// spread ...
const [key, value, ...otherProps] = ['name', 'email',true,'any',null,[],{}]
console.log(key)
console.log(value)
console.log(otherProps) // [true, 'any', null, Array(0), {…}]
// ------------------------- Object constructions -------------------------
/* Object.keys
* Returns an array of object keys
*  */
console.log(Object.keys(user3)) // ['name', 'email']

/* Object.values
* Returns an array of object values
*  */
console.log(Object.values(user3)) // ['Vladimir', 'any@gmail.com']

/* Object.entries
* Returns an array of arrays of keys and object values
*  */
console.log(Object.entries(user3)) // [['name', 'Vladimir'],['email', 'any@gmail.com']]

// loop for of
for (const value of Object.values(user3)) {
	console.log(value)
}

for (const [key, value] of Object.entries(user3)) {
	console.log(key, value)
}
