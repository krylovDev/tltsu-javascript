'use strict' // A strict mode that shows errors that JS can miss

// A descriptor is a special object that describes the behavior of an object property

// const user = {
// 	name: 'Vladimir',
// 	email: 'any@mail.ru'
// }
// delete user.name
// console.log(user)
// // ------------------------- defineProperty: Declaring a descriptor for a property -------------------------
// Object.defineProperty(user,'name', {
// 	writable: false, // Enables/disables change of the property value. The default is true
// 	configurable: true, // Enables/disables property deletion. The default is true
// 	enumerable: false // Whether the property is enumerated in the loop. The default is true
// })

// enumerable
// console.log(Object.keys(user)); // ['email']
//
// for (const key in user) {
// 	console.log(key) // email (name:enumerable false)
// }

// delete user.name

// Object.defineProperty(user,'email', {
// 	writable: true
// })

// writable
// user.name = 'Vladimir 2'
// console.log(user)
// user.email = 'new@mail.ru'
// console.log(user)


// const us = {
// 	name: 'Name'
// }
//
// us.name = 'new name'
// delete us.name
// console.log(us) // {}



function User(name, email) {
	this.name = name
	this.email = email

	Object.defineProperty(this,'name', {
		writable: false,
		configurable: false,
		enumerable: false
	})

	// If we want to apply the property to all at once:
	// Object.seal(this) // Prohibits adding and removing already existing object properties.
	// Object.freeze(this) // Prohibits adding, changing and deleting object properties.
}

const user = new User('Vladimir', 'any@mail.ru')
// user.name = 'new Key'
// user.newKey = 'new Value'
// delete user.name
const user2 = new User('Vladimir2', 'any2@mail.ru')
// user2.name = 'new Key'
// delete user2.name

console.log('user',user)
console.log('user getOwnPropertyDescriptors', Object.getOwnPropertyDescriptors(user))
// Allows you to see the object's property flags. Returns the value properties.
console.log(Object.getOwnPropertyDescriptor(user,'name'))
console.log('user getOwnPropertyNames', Object.getOwnPropertyNames(user))

const config = {
	isActive: true
}

Object.defineProperty(config,'isActive', {
	writable: false
})

// console.log('config getOwnPropertyDescriptors',Object.getOwnPropertyDescriptors(config))

// console.log('config getOwnPropertyNames',Object.getOwnPropertyNames(config))
