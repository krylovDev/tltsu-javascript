const headNode = document.head
const bodyNode = document.body
const htmlNode = document.documentElement

// console.log(headNode)
// console.log(bodyNode)
// console.log(htmlNode)


// firstElementChild, lastElementChild
// console.log(bodyNode.firstElementChild); // div
// console.log(bodyNode.lastElementChild); // div


// children
// console.log(bodyNode.children) // HTMLCollection(2) [div, script]
// HTMLCollection -> Array
// console.log(Array.from(bodyNode.children)) // Array (2)[div, script]
// console.log([...bodyNode.children]) // Array (2)[div, script]


// parentElement
// console.log(bodyNode.children[0]);
const div = bodyNode.firstElementChild
// console.log(div);
// console.log(div.parentElement);
const ul = div.children[0] // HTMLCollection(1) [ul]
// console.log(ul.children)

const middleLi = ul.children[1]
// console.log(middleLi)

// previousElementSibling
// console.log(middleLi.parentElement) // ul
// console.log(middleLi.previousElementSibling) // li: 1
// const firstLi = middleLi.previousElementSibling
// const lastLi = middleLi.nextElementSibling

// firstLi.textContent = '10'
// lastLi.textContent = '1000'




const h1 = document.createElement('h1')
h1.textContent = 'heading 1'
// div.prepend(h1)
// div.append(h1)
document.body.prepend(h1)













