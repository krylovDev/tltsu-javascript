const table = document.getElementsByTagName('table')[0]
// const caption = document.getElementsByTagName('caption')[0]
const caption = table.caption
// console.log(caption)
// caption.textContent = 'new Table'


const tHead = table.tHead
const tBody = table.tBodies[0]
const tFoot = table.tFoot

// rows -> HTMLCollection
const tHeadRows = tHead.rows
const tBodyRows = tBody.rows
const tFootRows = tFoot.rows
const bodyRows = [...tBodyRows]
bodyRows.forEach((row) => {
	// console.log(row.children) // HTMLCollection(3)[td, td, td]
	const rowsChildrenList = Array.from(row.children)
	rowsChildrenList.forEach((td) => {
		td.textContent = td.textContent + 0
	})
})
