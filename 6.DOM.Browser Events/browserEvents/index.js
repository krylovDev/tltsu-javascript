/* browser events:
* click
* submit - form
* input - input
* focus
* mouseover/mouseout
* mousedown/mouseup
* mousemove
* */

// const div = document.getElementById('div')

// div.addEventListener('click', (event) => {
	// console.log(event) // Event Object
	// console.log('event.target', event.target)
	// console.log('event.currentTarget', event.currentTarget)

	// event.target.classList.add('active')

	// event.target.classList.toggle('active')

// })



const checkbox = document.getElementById('checkboxId')
const input = document.getElementById('textId')
const button = document.querySelector('#buttonId')

const isAllowSubmit = {
	checkbox: false,
	input: false
}

function handleButtonState() {
	const isAllowButtonSubmit = Object.values(isAllowSubmit).every((booleanValue) => {
		return booleanValue === true
	})
	if (isAllowButtonSubmit) {
		button.removeAttribute('disabled')
	} else {
		button.setAttribute('disabled', 'true')
	}
}

checkbox.addEventListener('change', (event) => {
	isAllowSubmit.checkbox = event.target.checked
	handleButtonState()
})

input.addEventListener('input', (event) => {
	isAllowSubmit.input = Boolean(event.target.value)
	handleButtonState()
})




