// console.log('Star Wars')
// console.log('Film: "Star Wars"')
// console.log("Film: 'Star Wars'")
// console.log('Film: \'Star Wars\'')

// length
// console.log('Star Wars'.length) // 9


/* ---------- charAt ----------
Returns the specified character from a string.

Syntax: .charAt(index)

console.log('Star Wars'.charAt(0)) // S
console.log('Star Wars'.charAt('Star Wars'.length - 1)) //
*/


/* ---------- indexOf ----------
The indexOf() method returns the first index by which the given element can be found in the array.
Or -1 if there is no such index.

Syntax: .indexOf(searchString)

console.log('Star Wars'.indexOf('Wars')) // 5
console.log('Star Wars'.indexOf('Wars1')) // -1
*/


/* ---------- includes ----------
The includes() method checks if a string contains a given substring and returns true or false respectively.

Syntax: .indexOf(string)

console.log('Star Wars'.includes('Wars')) // true
console.log('Star Wars'.includes('Wars1')) // false
*/


/* ---------- startsWith/endsWith ----------
Checks if a string starts with the characters in parentheses

Syntax: .startsWith(string)

console.log('js@mail.ru'.startsWith('js')) // true
console.log('js@mail.ru'.endsWith('@mail.ru')) // true
console.log('js@mail.ru'.endsWith('@yandex.ru')) // false
*/


/* ---------- concat ----------
Concatenates strings and returns a new string.

Syntax: .concat(string or strings)

console.log('Star '.concat('Wars ','is ','awesome')) // Star Wars
*/


/* ---------- split ----------
Splits a string into an array of strings by separator

Syntax: .split(separator)

console.log('star@mail.ru'.split('@')) // ['star', 'mail.ru']
console.log('star@mail.ru'.split('.')) // ['star@mail', 'ru']
console.log('Star'.split('')) // ['S', 't', 'a', 'r']
*/


/* ---------- toUpperCase, toLowerCase ----------
Returns the string value converted to upper case ot to lower case resp.

Syntax: .toLowerCase/.toUpperCase()

console.log('star@mail.ru'.toUpperCase()) // STAR@MAIL.RU
console.log('Star'.toLowerCase()) // star
*/


/* ---------- replace, replaceAll ----------
returns a new string with some or all of the pattern matching replaced with the replacement. The pattern can be a string or a regular expression, and the placeholder can be a string or a function called on each match.

Syntax: .replace(searchString/RegExp, replaceString)

console.log('star@mail.ru'.replace('mail','yandex')) // star@yandex.ru
console.log('star@mail.ru any@mail.ru'.replace('mail','yandex')) // star@yandex.ru any@mail.ru
console.log('star@mail.ru any@mail.ru'.replaceAll('mail','yandex')) // star@yandex.ru any@mail.ru
*/


/* ---------- Equality == , === ----------
 == (compare bool)
console.log("'abc' == 'abc':",'abc' == 'abc') // true:  (Boolean('abc') == Boolean('abc')) -> true == true
console.log("1 == 1:",1 == 1) // true:  (Boolean(1) == Boolean(1)) -> true == true
console.log("'1' == 1:",'1' == 1) // true:  (!) (Boolean('1') == Boolean(1)) -> true == true

// True
// Falsy: 0, null, undefined, '', NaN

console.log('----------------------')

// === (99%)
console.log("'abc' === 'abc':",'abc' === 'abc') // true:  (Boolean('abc') === Boolean('abc')) -> true === true
console.log("1 === 1:",1 === 1) // true:  (Boolean(1) === Boolean(1)) -> true === true
console.log("'1' === 1:",'1' === 1) // true:  (!) (Boolean('1') === Boolean(1)) -> true === true*/

// console.log(1 + 'asd') // 1asd
// console.log(1 * 'asd') // NaN
// console.log(1 / 'asd') // NaN
// console.log(1 - 'asd') // NaN


/* ---------- Strings compare ----------
  console.log('any@mail.ru'.trim().toLowerCase() === 'anY@mail.ru'.trim().toLowerCase())
*/


/* ---------- trim ----------
Removes whitespace characters from the beginning and end of the string.

Syntax: .trim()

console.log('any@mail.ru       '.trim().toLowerCase() === 'anY@mail.ru'.toLowerCase())
* */
