/*
Regular expressions allow you to manipulate strings.
Search, Replace, AutoCorrect
Mainly used for form validation.
*/


// Syntax (patter, flag)
// Pattern - the condition by which the search should occur, string validation, a certain template for the string



/* ---------------------------------------- INFO HELPER ----------------------------------------

---------- Methods ----------
	.test - looking for an occurrence in a string. Returns boolean.
	.match - finds a match. Returns an array of matches.

---------- Flags ----------
	g - global search in string
	i - search is case insensitive

---------- Symbols ----------
  \d - all numbers
  \s - all spaces
  \w - Latin letters, numbers and _ or [a-zA-Z0-9_]

  \D - everything but numbers или [^0-9]
  \S - everything but spaces
  \W - everything except Latin letters, numbers and _ or [^a-zA-Z0-9_]

  . - any symbol
  [^abc] - everything except

---------- Anchors / Marking the beginning and end of a line ----------
  ^ - start of line
  $ - end of line

---------- Quantifiers / Repetition limiter ----------
  {4} - repeat 4 times
  * - 0 or more matches
  + - 1 or more matches
  ? - 0 to 1 matches

  /* -----------------------------------------------------------------------------------------------------
* */

// 1
// const regexp = new RegExp('learn')
// 2
// const regexp2 = /learn/g

// const firstStr = 'I learn JS'
// const secondStr = 'I learn CSS and i learn html'
// const thirdStr = 'I like cakes'

// .test(string) => boolean
// console.log(firstStr, regexp2.test(firstStr)) // true
// console.log(secondStr, regexp2.test(secondStr)) // true
// console.log(thirdStr, regexp2.test(thirdStr)) // false

// Finds a match. Returns an array of matches or null
// console.log(`match ${firstStr}:`, firstStr.match(regexp2)) // ['learn']
// console.log(`match ${secondStr}:`, secondStr.match(regexp2)) // ['learn', 'learn'] // without flag i it's return ['learn']
// console.log(`match ${thirdStr}:`, thirdStr.match(regexp2)) // null



// ---------- Search and replace ----------
// task: replace all 'learn' to 'know'
// console.log(firstStr.replace(regexp2, 'know'))
// console.log(secondStr.replace(regexp2, 'know'))
// console.log(thirdStr.replace(regexp2,'know'))


// ---------- Symbol class ----------
// task: search all numbers in string
// \d
// const regexp = /\d/gi
// const firstStr = '1234-1234-1234-1234'
// console.log(firstStr.match(regexp))

// task: replace all spaces to _
// const regexp = /\s/gi
// const firstStr = 'snake case is great'
// console.log(firstStr.replaceAll(regexp,'_'))


// task: replace all symbols to *
// const regexp = /\w/gi
// const firstStr = '1snake case_ is great'
// console.log(firstStr.replaceAll(regexp,'*'))


// ---------- Reverse character class ---------
// const regexp = /\W/gi
// const firstStr = '1snake case_ is great'
// console.log(firstStr.replaceAll(regexp,'*'))


// . - any symbol
// const regexp = /.at/g
// const cat = 'cat'
// const hat = 'hat'
// console.log(hat, regexp.test(hat))


// const regexp = /^c.t$/
// const firstStr = 'cat'
// console.log(firstStr, regexp.test(firstStr))


// task: Validate date format
// const regexp = /^\d\d-\d\d-\d\d\d\d \d\d:\d\d$/
// const firstStr = '01-01-1990 13:43'
// console.log(firstStr, regexp.test(firstStr))


// const regexp = /[a-z0-9]/g // [a-z0-9]
// const firstStr = 'milk'
// console.log(firstStr.match(regexp)) // ['m', 'i', 'l', 'k']


// Exclusion from the range
// const regexp = /[^lke]/g
// const firstStr = 'like'
// console.log(firstStr.match(regexp)) // [i]


// const regexp2 = /^[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4}/g
// const firstStr = '1234-1234-1234-1234' // true
// console.log(firstStr, regexp2.test(firstStr))


// task: validate password
// const regexp = /^\w{3,8}$/g
// const firstStr = 'qwertyuasdfgh' // false
// console.log(firstStr, regexp.test(firstStr))


// task: validate email
// any email with @ .com или .ru

// const regexp2 = /^\w+@\w+\.(com|ru)$/g
// const str1 = 'any@mail.ru' // true
// console.log(str1, regexp2.test(str1))
