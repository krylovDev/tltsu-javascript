// console.log('Hello') // devTools in browser (F12)
// alert('Hello') // alert window
// confirm('Hello') // confirm window

// ---------- Primitive Data types: number, string, boolean, null, undefined, bigint, symbol. ----------

// bigint 1234567890123456789012345678901234567890n

// typeof operator. Returns the type of the argument
console.log('typeof 12:', typeof(12))
console.log('typeof 1234567890123456789012345678901234567890n:', typeof 1234567890123456789012345678901234567890n)
console.log('typeof "str":', typeof("str"))
console.log('typeof true:', typeof true)
console.log('typeof null:', typeof null) // object(!) Official error in JS

// undefined
let a
console.log('typeof a:',typeof a) // undefined

// ---------- Reference Data types: object. ----------
// objects, arrays, functions
console.log('typeof {}:', typeof {}) // object
console.log('typeof []:', typeof []) // object
console.log('typeof Fn:', typeof function() {}) // function(!) Official error in JS
