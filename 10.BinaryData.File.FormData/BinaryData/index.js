// const arrayBuffer = new ArrayBuffer(1024)
// console.log(arrayBuffer)
//
// const uint8 = new Uint8Array(arrayBuffer)
// uint8[0] = 231
// console.log(uint8)


// encoding
const str = 'Hello,JS!'
const encoder = new TextEncoder() // '' -> Uint8Array
const binaryStr = encoder.encode(str) // Uint8Array [72, 101, 108, 108, 111]
console.log('binaryStr',binaryStr)

// decoding
const decoder = new TextDecoder()
const decodedStr = decoder.decode(binaryStr)
console.log('decodedStr',decodedStr) // Hello,JS!

const unit8ArrayStr = new Uint8Array([72, 101, 108, 108, 111])
console.log(decoder.decode(unit8ArrayStr)) // Hello



// BLOB - Binary Large Object
let blob = new Blob(['Hello,JS!'], {
	type: 'text/plaint'
})

const url = URL.createObjectURL(blob)
console.log('url', url)

document.getElementById('downloadId').href = url

// URL.revokeObjectURL(url)
// blob = null



// base64url
const reader = new FileReader()
console.log('1',reader)
reader.readAsDataURL(blob)
reader.onload = () => {
	console.log('2',reader)
	document.getElementById('base64').href = reader.result
}





















