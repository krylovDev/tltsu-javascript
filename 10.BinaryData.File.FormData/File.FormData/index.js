const input = document.getElementById('fileInput')

// input.onchange = () => {
// 	const file = input.files[0]
// 	const image = document.createElement('img')
// 	image.src = URL.createObjectURL(file)
// 	document.querySelector('.gallery').prepend(image)
// }



// ===================================== + localStorage =====================================
/*
const imagesArray = localStorage.getItem('gallery')
	? JSON.parse(localStorage.getItem('gallery'))
	: []

function addImageToGallery(base64img) {
	const image = document.createElement('img')
	image.src = base64img
	document.querySelector('.gallery').prepend(image)
}

imagesArray.forEach(addImageToGallery)

input.onchange = () => {
	const reader = new FileReader()
	const file = input.files[0]
	reader.readAsDataURL(file)
	reader.onload = () => {
		const base64url = reader.result
		imagesArray.push(base64url)
		localStorage.setItem('gallery', JSON.stringify(imagesArray))
		addImageToGallery(base64url)
	}
}
*/



// ===================================== + API =====================================
const uploadButton = document.getElementById('uploadButton')
const IMG_BB = {
	key: '2b29570963d670be0f8ff36586c47016',
	url: 'https://api.imgbb.com/1/upload'
}

function addImageToGallery(url) {
	const image = document.createElement('img')
	image.src = url
	document.querySelector('.gallery').prepend(image)
}

const imagesArray = localStorage.getItem('gallery')
	? JSON.parse(localStorage.getItem('gallery'))
	: []

imagesArray.forEach(addImageToGallery)

/*
function handleClickUploadButton() {
	const formData = new FormData()
	formData.set('key',IMG_BB.key)
	formData.set('image',input.files[0])

	fetch(IMG_BB.url, {
		method: 'POST',
		body: formData
	})
		.then((response) => response.json())
		.then((result) => {
			console.log('result', result)
			const imageUrl = result.data.display_url
			imagesArray.push(imageUrl)
			addImageToGallery(imageUrl)
			localStorage.setItem('gallery', JSON.stringify(imagesArray))
		})
		.catch((error) => console.error(error.message))
}
*/

async function handleClickUploadButton() {
	const formData = new FormData()
	formData.set('key',IMG_BB.key)
	formData.set('image',input.files[0])

	try {
		const response = await fetch(IMG_BB.url, {
			method: 'POST',
			body: formData
		})

		const result = await response.json()
		const imageUrl = result.data.display_url
		imagesArray.push(imageUrl)
		addImageToGallery(imageUrl)
		localStorage.setItem('gallery', JSON.stringify(imagesArray))
	} catch (error) {
		console.error(error.message)
	}

}

uploadButton.addEventListener('click', handleClickUploadButton)
























