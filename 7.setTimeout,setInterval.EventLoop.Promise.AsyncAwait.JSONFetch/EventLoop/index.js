/* EventLoop
* [callstack] [queuestack] [WebApi]
* 1. Sync tasks -> Callstack
* 2. Async tasks -> Queuestack -> [2.1 micro-tasks: 99% Promise; 2.2 macro-tasks: timeouts, intervals]
* 3. browser events ->  WebApi
* */

// button.addEventListener('click', () => {})
const button = document.getElementById('btn')

function mainFunc() {
	console.log('mainFunc 1')
	setTimeout(() => console.log('timeout 1'), 100) // 0.00000000000004
	secondFunc()
	// button.addEventListener('click',() => {})
	console.log('mainFunc 2')
}

function secondFunc() {
	console.log('secondFunc 1')
	setTimeout(() => console.log('timeout 2'), 0)
	console.log('secondFunc 2')
}

mainFunc()


