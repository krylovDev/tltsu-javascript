


// ---------------------------- timeout ----------------------------

// const timeoutCallback = () => {
// 	console.log('timeoutCallback')
// }

// setTimeout(() => {
// 	console.log('timeout')
// }, 2000)

// setTimeout(timeoutCallback, 3000)

// const timeoutId = setTimeout(timeoutCallback, 3000)
// console.log('timeoutId', timeoutId) // timeoutId 2


// let recursiveTimeout = setTimeout(function tick() {
// 	console.log('tick')
// 	recursiveTimeout = setTimeout(tick, 1000)
// },1000)
//
//
// setTimeout(() => {
// 	clearTimeout(recursiveTimeout)
// 	console.log('recursiveTimeout stop')
// }, 5000)






// ---------------------------- interval ----------------------------
// const interval = setInterval(() => {
// 	console.log('tick')
// },1000)
//
// console.log(interval) // id
//
//
// setTimeout(() => {
// 	console.log('stop')
// 	clearInterval(interval)
// }, 5000)





const button = document.getElementById('buttonId')

button.addEventListener('click', (() => {
	let interval
	let isActive = false

	return () => {
		if (isActive) {
			clearInterval(interval)
			button.classList.remove('active')
		} else {
			interval = setInterval(() => {
				button.classList.toggle('active')
			}, 1000)
		}
		isActive = !isActive
	}

})())







