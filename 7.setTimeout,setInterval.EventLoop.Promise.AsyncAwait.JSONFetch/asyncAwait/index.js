// sync
function sum(a,b) {
	return a+b
}

console.log('sync', sum(10,1))

// async
function promiseSum(a,b) {
	return new Promise((resolve, reject) => resolve(a+b))
}

promiseSum(20,1)
	.then((result) => console.log('async promise',result)) // 21

// ASYNC/await
async function asyncSum(a,b) {
	return a + b
}

asyncSum(20,5)
	.then((result) => console.log('async promise',result)) // 25


// ASYNC/AWAIT -> Promise
async function asyncAwaitSum(a,b) {
	const result = await a+b
	console.log(result) // 30
	return result
}

console.log(asyncAwaitSum(10,20))



// ECMASCRIPT 6 ES6: class, ..., async/await, try/catch, let const ....
async function getUsers() {
	try {
		const response = await fetch('https://js2onplaceholder.typicode.com/users') // object Response
		const usersList = await response.json() // object -> JSON
		console.log(usersList) // [{},{}]
	} catch (error) {
		console.error(`Error -> ${error.message}`)
	}
}

getUsers()

// const getUsers = new Promise((resolve, reject) => {
// 	fetch('https://jsonplaceholder.typicode.com/users')
// 		.then((response) => {
// 			// console.log('1',response) // object Response
// 			return response.json()
// 		})
// 		.then((usersList) => {
// 			// console.log('2', usersList)
// 			resolve(usersList) // [{},{},...]
// 		})
// 		.catch((error) => {
// 			reject(error.message)
// 		})
// })
