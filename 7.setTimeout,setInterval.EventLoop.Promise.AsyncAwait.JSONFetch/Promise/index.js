// Promise
// PromiseStates: pending, fulfilled, rejected

// const promise = new Promise((resolve, reject) => {
// 	let result = 0
// 	for (result; result < 1e9; result++) {}
// 	resolve(result)
// })
//
// const promiseError = new Promise((resolve, reject) => {
// 	let result = 0
// 	for (result; result < 1e9; result++) {}
// 	reject(result)
// })

// console.log(promise)
// console.log(promiseError)

// .then
// .catch
// .finally

// promise
// 	.then((response) => {
// 		console.log('response', response)
// 	})
// 	.catch((error) => {
// 		console.error(`promise: ${error}`)
// 	})
// 	.finally(() => {
// 		console.log('promise .finally()')
// 	})
//
// promiseError
// 	.then((response) => {
// 		console.log('response', response)
// 	})
// 	.catch((error) => {
// 		console.error(`promiseError: ${error}`)
// 	})
// 	.finally(() => {})









// const promiseWIthChain = new Promise((resolve, reject) => {
// 	resolve(100)
// })
//
// promiseWIthChain
// 	.then((response) => {
// 		console.log('1.then',response) // 100
// 		return response / 5 // 100 / 5
// 	})
// 	.then((response) => {
// 		console.log('2.then',response) // 20
// 		return response * 5 // 20 * 5
// 	})
// 	.then((response) => {
// 		console.log('3.then', response) // 100
// 	})






const getUsers = new Promise((resolve, reject) => {
	fetch('https://jsonplaceholder.typicode.com/users')
		.then((response) => {
			// console.log('1',response) // object Response
			return response.json()
		})
		.then((usersList) => {
			// console.log('2', usersList)
			resolve(usersList) // [{},{},...]
		})
		.catch((error) => {
			reject(error.message)
		})
})

const getTodos = new Promise((resolve, reject) => {
	fetch('https://jsonplaceholder.typicode.com/todos')
		.then((response) => {
			// console.log('1',response) // object Response
			return response.json()
		})
		.then((todosList) => {
			// console.log('2', usersList)
			resolve(todosList) // [{},{},...]
		})
		.catch((error) => {
			reject(error.message)
		})
})

function renderUsers(array) {
	for (const user of array) {
		const div = document.createElement('div')
		div.textContent = user.name
		document.body.prepend(div)
	}
}

function renderTodos(array) {
	for (const todo of array) {
		const div = document.createElement('div')
		div.textContent = todo.title
		document.body.prepend(div)
	}
}

function logError(error) {
	const h1 = document.createElement('h1')
	h1.textContent = error
	document.body.prepend(h1)
}

// getUsers
// 	.then((usersList) => {
// 		// console.log('getUsers', usersList)
// 		renderUsers(usersList)
// 	})
// 	.catch((error) => {
// 		// console.error('getUsers', error)
// 		logError(error)
// 	})
//
//
// getTodos
// 	.then((result) => {
// 		renderTodos(result)
// 	})
// 	.catch((error) => {
// 		logError(error)
// 	})

Promise.all([
	getUsers,
	getTodos
]).then(([users, todos]) => {
	renderUsers(users)
	renderTodos(todos)
})
	.catch((error) => logError(error))


























































