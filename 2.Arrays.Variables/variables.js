// var let const
var str = undefined

// console.log(str) // undefined
var str = 'string' // =
// console.log(str) // 'string'

// let str1 = 'string'
// const str2 = 'string'

// console.log(str1) // Cannot access 'str1' before initialization
let str1 = 'let string'
str1 = 'new variable'
console.log(str1) // 'new variable'

// console.log(str2) // Cannot access 'str2' before initialization
const str2 = 'const string'
// str2 = 'new const variable'
console.log(str2) // 'const string'