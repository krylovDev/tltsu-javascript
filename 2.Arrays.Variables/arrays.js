// const array = [11,22,33,44,55]
// console.log(array) // [11,22,..] <- Array <- Object
// console.log(typeof [11,22,33,44,55]) // object
// console.log(array[1]) // 22

// const obj = {
// 	0: 11,
// 	1: 22,
// 	2: 33,
// }
// console.log(obj) // {} <- Object
// console.log(obj[1]) // 22


// ------------ push ------------
// The push() method adds the specified elements to the end of an array and returns the new length of the array.
// const arr = [11,22]
// console.log(arr) // [11, 22]
// arr.push(33)
// console.log(arr) // [11, 22, 33]


// ------------ pop ------------
// The pop() method removes the last element from an array and returns that element. This method changes the length of the array.
// const arr = [11,22,33]
// console.log(arr) // [11, 22, 33]
// arr.pop()
// console.log(arr) // [11, 22]


//  ------------ unshift ------------
// The unshift() method adds the specified elements to the beginning of an array and returns the new length of the array.
// const arr = [11,22,33]
// console.log(arr) // [11, 22, 33]
// arr.unshift(44)
// console.log(arr) // [44, 11, 22, 33]


// ------------ shift ------------
// The shift() method removes the first element from an array and returns that removed element. This method changes the length of the array.
// const arr = [11,22,33]
// console.log(arr) // [11, 22, 33]
// arr.shift()
// console.log(arr) // [22, 33]


// ------------ splice ------------
// The splice() method changes the contents of an array by removing or replacing existing elements and/or adding new elements in place.
// const arr = [11,22,33,true,{},12,55]
// arr.splice(1,3) // [11, {…}, 12, 55]
// arr.splice(1,2) // [11, 55]
// console.log(arr) // [11, 55]


// ------------ slice ------------
// The slice() method returns a shallow copy of a portion of an array into a new array object selected from start to end (end not included) where start and end represent the index of items in that array. The original array will not be modified.
// const arr = [11,22,33,true,null]
// console.log(arr) // [11,22,33,true,null]
// console.log(arr.slice(1, 3)); // [22, 33]
// console.log(arr.slice()); // [11,22,33,true,null]
// console.log(arr) // [11,22,33,true,null]
//
// const copy = [...arr] // operator spread (...)
// const copy2 = [...[11,22,33,true,null]]
// console.log('...',copy)
// console.log('...',copy2)


// ------------ concat ------------
// The concat() method is used to merge two or more arrays. This method does not change the existing arrays, but instead returns a new array.
// const arr = [11,22,33,44]
// console.log(arr)
// console.log(arr[arr.length - 1]) // 44
// console.log(arr.at(0)) // 11
// console.log(arr.at(-1)) // 44

// const arr2 = [33,44]
// console.log(arr.concat(arr2)) // [11, 22, 33, 44]
// console.log([...arr,...arr2]) // [11, 22, 33, 44]
// console.log([...arr2,...arr]) // [33, 44, 11, 22]
// const result = []
// result.push(arr)
// console.log(result[0]) // [11, 22]


// ------------ join ['a','b','c'] -> 'a,b,c' ------------
// The join() method creates and returns a new string by concatenating all of the elements in an array (or an array-like object), separated by commas or a specified separator string. If the array has only one item, then that item will be returned without using the separator.
// const arr = ['a','b','c']
// console.log(arr.join())
// console.log(arr.join().toUpperCase())


// console.log(Array.isArray([33, 44])); // true
// console.log(Array.isArray(1)); // false
// console.log(Array.isArray('')); // false
// console.log(Array.isArray(true)); // false
//
//
// function isArray(arr) {
// 	if (Array.isArray(arr)) { // true
// 		console.log('Array!')
// 	} else { // false
// 		console.log('Error!')
// 	}
// }
//
// isArray(true)



// const arr = [11,22,33,44,55]
// console.log(arr)
// console.log(arr[0]) // 11
// console.log(arr[1]) // 22
// console.log(arr[2]) // 33
// console.log(arr[6]) // undefined

// ------------ for ------------
// for (let i=0; i<10; i++) {
// 	console.log(`${i} - ${arr[i]}`)
// }

// ------------ for of ------------
// for (const value of arr) {
// 	console.log('value', value)
// }



// ------------ .forEach(() => {}) ------------
// The forEach() method executes a provided function once for each array element.

// const arr = [11,22,33,44,55]
//
// arr.forEach((value, index, initialArray) => {
// 	console.log('value', value) // 11
// 	console.log('index', index) // 0
// 	console.log('initialArray', initialArray) // [11,22,33,44,55]
// })
//
// const newArray = arr.forEach((num) => {
// 	return num * 100
// })
// console.log(newArray)


// ------------ .map(() => {}) ------------
// The map() method creates a new array populated with the results of calling a provided function on every element in the calling array.
// const arr = [11,22,33,44,55]
// function anyFunc(value) {
// 	return value * 10
// }
//
// const newArray1 = arr.map((value) => {
// 	return value * 10
// })
// const newArray = arr.map(anyFunc) // fn(){}
//
// console.log(arr) // [11, 22, 33, 44, 55]
// console.log(newArray) // [110, 220, 330, 440, 550]
// console.log(newArray1) // [110, 220, 330, 440, 550]

// ------------ filter(() => {}) ------------
// The filter() method creates a shallow copy of a portion of a given array, filtered down to just the elements from the given array that pass the test implemented by the provided function.
// const arr = [1,-3,132,-5,2]
//
// const filteredArr = arr.filter((value) => {
// 	// if (value > 0) {
// 	// 	return value
// 	// }
//
// 	return value > 0
// })
//
// console.log('filteredArr', filteredArr)
// console.log('arr',arr)


// const cardItemList = [
// 	{
// 		name: 'iphone7',
// 		price: 3000
// 	},
// 	{
// 		name: 'iphone11',
// 		price: 2000
// 	},
// 	{
// 		name: 'iphone9',
// 		price: 9000
// 	},
// ]
//
// // 3000+
//
// console.log(cardItemList.filter((cardItem) => {
// 	return cardItem.price <= 3000
// 	// return cardItem.name.endsWith('9')
// }))


// ------------ sort ------------
// The sort() method sorts the elements of an array in place and returns the reference to the same array, now sorted.
// const arr = [1,-3,132,-5,2]
// console.log(arr.sort((prevElement, nextElement) => {
//
// 	return prevElement - nextElement // [-5, -3, 1, 2, 132]
// 	// return nextElement - prevElement // [132, 2, 1, -3, -5]
//
// 	// if (prevElement > nextElement) {
// 	// 	return 1
// 	// }
// 	//
// 	// if (prevElement < nextElement) {
// 	// 	return -1
// 	// }
// 	//
// 	// if (prevElement === nextElement) {
// 	// 	return 0
// 	// }
//
// }))


// function getSortedData(data, dir) {
// 	if (dir === 'asc') {
// 		return data.sort((prevElement, nextElement) => {
// 			return prevElement - nextElement
// 		})
// 	}
//
// 	if (dir === 'desc') {
// 		return data.sort((prevElement, nextElement) => {
// 			return nextElement - prevElement
// 		})
// 	}
// }
//
// const data = getSortedData(arr, 'desc') // 'asc' | 'desc'
// console.log(data)





// -------- .find(() => {}) ----------
// The find() method returns the first element in the provided array that satisfies the provided testing function.
// const userList = [
// 	{
// 		name: 'user1',
// 		id: 3
// 	},
// 	{
// 		name: 'user11',
// 		id: 2
// 	},
// 	{
// 		name: 'user9',
// 		id: 9
// 	},
// ]

// const myId = 91
//
//
// const findedUser = userList.find((user) => {
// 	// if (user.id === myId) { // true/false
// 	// 	return user
// 	// }
// 	return user.id === myId
// })
// console.log(findedUser)
//
//
//
//
// if (findedUser) { // Boolean: true/false
// 	console.log('if')
// } else {
// 	console.log('else')
// }
