

// function mainFunc(callback) {
// 	for (let i=0; i<=100; i++) {
// 		console.log(`${i}%`)
// 	}
// 	const response = {
// 		name: 'Vladimir',
// 		job: 'React TS Developer'
// 	}
//
// 	callback(response) // callbackFunc(response)
// 	// console.log('response from server', response)
// }
//
// function callbackFunc(data) {
// 	console.log('response from server', data)
// }
//
//
// mainFunc(callbackFunc)


// const mark = 'BMW'
//
// function makeCar() {
// 	const mark = 'Volvo'
//
// 	return function() {
// 		console.log(mark)
// 	}
// }
//
// const car = makeCar()
// car() // Volvo




// IIFE - Immediately Invoked Function Expression
const notIIFE = () => {
	console.log('notIIFE')
}


(() => {
	// console.log('IIFE')
})()


const counter = (() => { // IIFE
	let count = 0

	return function() {
		count = count + 1 // 0+1:1, 1+1:2
		console.log(`${count}`)
	}
})()

// counter() // 1
// counter()
// counter()
// counter()
// counter()
// counter()
// counter()
// counter()
// counter()







const getFactorial = (() => {
	const cash = {}

	return (num) => {
		// console.log(`num: ${num}`)
		if (cash[num]) {
			console.log('cash', cash)
			return cash[num]
		} else {
			let fact = 1
			for (let i=1; i <= num; i++) {
				fact = fact * i
			}
			cash[num] = fact
			return fact
		}
	}
})()

console.log(getFactorial(5)); // 120
console.log(getFactorial(5)); // 120
console.log(getFactorial(10)); // 3628800
console.log(getFactorial(10)); // 3628800
console.log(getFactorial(10)); // 3628800

/*
* {
*   5: 120,
*   10: 3628800
* }
* */





















