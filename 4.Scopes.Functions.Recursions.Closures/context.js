// this

const obj = {
	name: 'my-obj',
	logThis: function() {
		console.log('this', this) // obj {}
	},
	childObj: {
		name: 'child-obj',
		email: 'child-obj@gmail.com',
		logThis: function() {
			// console.log('this', this) // childObj {}
			console.log('this.email', this.email) // this.name child-obj@gmail.com
		},
		arrowLogThis: () => { // arrow: lost context (this)
			// console.log('this', this) // window
			console.log('this.email', this.email) // this.email    window.email
		}
	}
}

// obj.logThis()
obj.childObj.logThis()
obj.childObj.arrowLogThis()


// console.log(this) // window
