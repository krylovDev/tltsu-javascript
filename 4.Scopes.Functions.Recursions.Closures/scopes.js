// var var1 = undefined

// global scope

// const globalConst = 'globalConst'
//
// // functional scope
// function scriptFunc() {
// 	const scriptConst = 'scriptConst'
// 	console.log('scriptFunc:',globalConst) // scriptFunc: globalConst
// 	console.log('scriptFunc:',scriptConst) // scriptFunc: scriptConst
//
// 	function childFunc() {
// 		const childConst = 'childConst'
// 		console.log('childFunc globalConst:',globalConst) // childFunc globalConst: globalConst
// 		console.log('childFunc scriptConst:',scriptConst) // childFunc scriptConst: scriptConst
// 		console.log('childFunc childConst:',childConst) // childFunc childConst: childConst
// 	}
// 	childFunc()
// }
//
// scriptFunc()


// const globalConst = 'globalConst'
//
// function mainFunc() {
// 	const mainConst = 'mainConst'
// 	console.log(mainConst)
// }
//
// // console.log(mainConst) // Error
//
// mainFunc()



// ------------------ block: for, if/else ------------------
// for (let i=0; i<3; i++) {
// 	console.log(i)
// }
//
// const i = 100
//
// console.log(i) // 100
//
//
// if (1+1) {
// 	const ifConst = 'ifConst'
// 	console.log(ifConst)
// } else {
//
// }
//
// console.log(ifConst)



var var1 = 1000
function varFunc() {
	console.log(var1) // 1000
}

console.log(var1) // 1000

varFunc()










