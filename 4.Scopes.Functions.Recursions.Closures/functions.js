// 1 function declaration
function declaredFunc(message, number) {
	// console.log('declaredFunc')
	// console.log('message',message)
	// console.log('number',number)
	console.log(`params: ${message} ${number}`)
}
// declaredFunc() // declaredFunc
declaredFunc('declaredFunc', '1') // declaredFunc 1
declaredFunc('declaredFunc')
declaredFunc('declaredFunc', '3')

// 2 function expression
const expressionFunc = function() {
	console.log('expressionFunc')
}
// expressionFunc() // expressionFunc

// 3 arrow func
const arrowFunc = () => {
	console.log('arrowFunc')
}
// arrowFunc() // arrowFunc



