// function firstFunc() {
// 	console.log('firstFunc 1')
// 	secondFunc()
// 	console.log('firstFunc 2')
// }
//
// function secondFunc() {
// 	console.log('secondFunc')
// 	console.log('secondFunc')
// 	console.log('secondFunc')
// 	console.log('secondFunc')
// 	console.log('secondFunc')
// 	console.log('secondFunc')
// }
// firstFunc()


// function counter(start, end) {
// 	for (let i = start; i <= end; i++) {
// 		console.log(i) // 2 3 4 5 6 7
// 	}
// }
//
// counter(2,7)


// function counterWithRecursion(start, end) {
// 	const nextNum = start + 1 // 2+1:3, 3+1:4, ...
// 	console.log(nextNum) // 3....7
//
// 	if (nextNum < end) { // 3 < 7... 6 < 7
// 		counterWithRecursion(nextNum, end) // counterWithRecursion(3,7), counterWithRecursion(4,7) ...  counterWithRecursion(6,7)
// 	}
// }
// counterWithRecursion(2,7)




// pow
// function getPow(num, pow) {
// 	if (pow !== 1) { // pow:3, pow:2
// 		console.log('pow', pow)
// 		return num * getPow(num, pow - 1) // pow: 3, pow:2,
// 	} else {
// 		console.log('num', num)
// 		return num // 2
// 	}
// }
//
// console.log(getPow(2,3)) // 8

/*
* 1) num: 2, pow:3
*   return 2 * getPow(2, 2) -> 2*4
*     2) num: 2, pow: 2
*       return 2 * getPow(2, 1) -> 2*2
*         3) num:2, pow: 1
*             return 2
* */





const data = {
	a: [10,2,3], // 15 + (15 + (15+15))
	b: {
		c: [10,2,3], // 15 + (15+15)
		d: {
			e: [10,2,3], // 15
			f: [10,2,3], // 15
		}
	}
}
// Object.values -> []
// reduce

// function sumArrayNums(data) {
// 	if (Array.isArray(data)) { // array
// 		return data.reduce((acc, num) => {
// 			console.log('array acc', acc)
// 			return acc + num
// 		},0)
// 	} else { // object
// 		return Object.values(data).reduce((acc, num) => {
// 			// console.log('object acc', acc)
// 			return acc + sumArrayNums(num)
// 		},0)
// 	}
// }

const getOne = () => {
	return 1
}

const getOne1 = () => 1

function sumArrayNums(data) {
	if (Array.isArray(data)) { // array
		return data.reduce((acc, num) => acc + num,0)
	} else { // object
		return Object.values(data).reduce((acc, num) => acc + sumArrayNums(num),0)
	}
}

console.log(sumArrayNums(data));












































