export const logError = (error) => {
	const h2 = document.createElement('h2')
	h2.textContent = `Failed: ${error.message}`
	document.body.prepend(h2)
}
