export const createFetch = (url) => (callback, errorCallback) => {
	fetch(url)
		.then((response) => response.json())
		.then((result) => callback(result))
		.catch((error) => errorCallback(error))
}
