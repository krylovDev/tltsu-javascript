import { jsonPlaceHolder } from "./api/jsonPlaceHolder.js";
import {swapi} from './api/swapi.js'
import {renderMainLinks, renderFilmsList} from "./components/swapiComponentsList.js";
import {logError} from './utils/logError.js'


swapi.getMain(renderMainLinks, logError)
swapi.getFilms(renderFilmsList, logError)

jsonPlaceHolder.getUsers(console.log, logError)
