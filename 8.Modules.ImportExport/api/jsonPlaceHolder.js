import { USERS_URL } from "../contants/jsonPlaceHolder.js";
import { createFetch } from "../utils/fetchUtils.js";

export const jsonPlaceHolder = {
	getUsers: createFetch(USERS_URL)
}
