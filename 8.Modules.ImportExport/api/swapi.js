import {createFetch} from '../utils/fetchUtils.js'
import {FILMS_URL, MAIN_URL} from '../contants/swapi.js'

export const swapi = {
	getMain: createFetch(MAIN_URL),
	getFilms: createFetch(FILMS_URL)
}
