// create
function createMainLinks(object) {
	const container = document.createElement('div')

	Object.keys(object).forEach((key) => {
		const divElement = document.createElement('div')
		divElement.innerHTML = `
		<span>${key}</span>: <a
		target="_blank"
		href='${object[key]}'
		>${object[key]}</a>
		`
		container.prepend(divElement)
	})

	return container
}

// render
export function renderMainLinks(obj) {
	const container = createMainLinks(obj)
	document.body.prepend(container)
}



// create film (<li>)
const createFilm = (title, url) => {
	const li = document.createElement('li')

	const titleElement = document.createTextNode(`${title} - `)
	li.prepend(titleElement)

	const linkElement = document.createElement('a')
	linkElement.href = url
	linkElement.text = url

	li.append(linkElement)

	return li
}

// create films list
const createFilmsList = (filmsArray) => {
	const filmsList = document.getElementById('films')
	filmsArray.forEach((film) => {
		const newFilm = createFilm(film.title, film.url)
		filmsList.append(newFilm)
	})
	return filmsList
}

// render
export const renderFilmsList = (filmsArray) => {
	const filmsList = createFilmsList(filmsArray.results)
	document.body.prepend(filmsList)
}
