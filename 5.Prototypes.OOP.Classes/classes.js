// class Person {
// 	constructor(name) {
// 		this.name = name
// 	}
// 	sayHi() {
// 		console.log(`my name is ${this.name}`)
// 	}
// }
//
// const person = new Person('Vladimir')
// // console.log(person.name) // Vladimir
// // person.sayHi() // my name is Vladimir
//
// const person2 = new Person('Vladimir 2')
// // console.log(person2)
// // console.log(person2.name) // Vladimir 2
// // person2.sayHi() // my name is Vladimir 2
//
//
// class Teacher extends Person {
// 	constructor(subject, name) {
// 		super(name) // Person: constructor()
// 		this.subject = subject
// 		// this.name = name
// 	}
// }
//
// const teacher = new Teacher('JS', 'Vladimir 2')
// console.log(teacher.subject) // JS
// // teacher.sayHi() // my name is Vladimir 2
// console.log(teacher.name)
//
// const teacher2 = new Teacher('HTML', 'Vladimir 3')
// console.log(teacher2.subject); // HTML
// // teacher2.sayHi() // my name is Vladimir 3




// ------------------ static method ------------------

class Person {
	static countOfPersons = 0
	constructor(name, phone) {
		this.name = name
		this.phone = phone
		Person.countOfPersons++
	}
}
const person = new Person('name 1', '11-2')
const person2 = new Person('name 1', '11-2')
const person3 = new Person('name 1', '11-2')
const person4 = new Person('name 1', '11-2')

class Teacher extends Person {
	static staticSayHi() {
		console.log('static hi')
	}
	static countOfTeachers = 0

	constructor(subject, name, phone) {
		super(name, phone);
		Teacher.countOfTeachers++
	}
}
console.log(Teacher.countOfPersons) // 4

const teacher = new Teacher('JS','Vladimir','1-22-333')
const teacher2 = new Teacher('JS','Vladimir','1-22-333')
const teacher3 = new Teacher('JS','Vladimir','1-22-333')
const teacher4 = new Teacher('JS','Vladimir','1-22-333')
const teacher5 = new Teacher('JS','Vladimir','1-22-333')
// teacher.staticSayHi() // Error
// Teacher.staticSayHi() // static hi
console.log(Teacher.countOfTeachers) // 5

console.log(Teacher.countOfPersons) // 9




// js
function sum(a,b) {
	return a + b
}

sum(4,5)

































