// const person = {
// 	name: 'Vladimir',
// 	sayHi() {
// 		console.log(`my name is ${this.name}`)
// 	},
// 	sayBye() {
// 		console.log('Bye')
// 	}
// }

// person <- teacher
// const teacher = {
// 	__proto__: person,
// 	subject: 'JS',
// 	name: 'Vladimir 1',
// 	sayHi() {
// 		// this.__proto__.sayHi() // person.sayHi()
// 		console.log(`subject: ${this.subject}`)
// 	}
// }

// person <- teacher <- director
// const director = {
// 	__proto__: teacher,
// 	name: 'Vladimir 2',
// 	subject: 'Set rules'
// }

// console.log(teacher.name); // Vera
// teacher.sayHi()
// director.sayHi() // console.log(`subject: ${director.subject}`)
// director.sayBye()


// const global = 'global'
// function a() {
// 	console.log(global);
// }





// const person = {
// 	name: 'Vladimir',
// 	sayHi() {
// 		console.log(`my name is ${this.name}`)
// 	},
// 	sayBye() {
// 		console.log('Bye')
// 	}
// }
//
// const teacher = {
// 	__proto__: person,
// 	subject: 'JS',
// 	name: 'Vladimir2',
// 	sayHi() {
// 		console.log(`my name is ${this.name}. my subject is ${this.subject}`)
// 	}
// }
// // teacher.sayHi()
//
// // Object.keys
// console.log(Object.keys(teacher)) // ['subject', 'name', 'sayHi']
//
// // for in
// for(let key in teacher) {
// 	console.log(key) // subject name sayHi sayBye (keys with prototypes keys)
// }




// const person = {
// 	name: 'Vladimir',
// 	sayHi() {
// 		console.log(`my name is ${this.name}`)
// 	},
// 	sayBye() {
// 		console.log('Bye')
// 	}
// }

// Object <- person <- teacher
// const teacher = {
// 	// __proto__: person,
// 	subject: 'JS',
// 	name: 'Vladimir2',
// 	sayHi() {
// 		console.log(`my name is ${this.name}. my subject is ${this.subject}`)
// 	}
// }

// Object.setPrototypeOf(teacher, person)
// teacher.sayBye() // Bye

// Object <- person <- teacher <- director
// const director = {
// 	// __proto__: teacher,
// 	name: 'Vladimir 2',
// 	subject: 'Set rules'
// }
// Object.setPrototypeOf(director, teacher)
// director.sayBye() // Bye

// console.log(Object.getPrototypeOf(director)); // teacher

// console.log(Object.getPrototypeOf({})) // Object <- {}




// ----------------------------------------------------------------

// const person = {
// 	name: 'Vladimir',
// 	sayHi() {
// 		console.log(`my name is ${this.name}`)
// 	},
// 	sayBye() {
// 		console.log('Bye')
// 	}
// }
//
// function Teacher(subject) {
// 	this.subject = subject
// }
// Teacher.prototype = person
//
//
// const teacher = new Teacher('JS')
// const teacher2 = new Teacher('HTML')
//
// console.log(Object.getPrototypeOf(teacher)); // person
// console.log(Object.getPrototypeOf(teacher2)); // person

// ----------------------------------------------------------------

function Person(name) {
	this.name = name
	this.sayHi = function() {
		console.log(`my name is ${this.name}`)
	}
	this.sayBye = function() {
		console.log('Bye')
	}
}

// const person = new Person('Vladimir 23')
// const person2 = new Person('Vladimir 2')

function Teacher(name, subject) {
	Person.call(this, name) // .call() -> context(this) Person
	this.subject = subject
}
// Teacher.prototype = person


const teacher = new Teacher('Vladimir 3','JS')
const teacher2 = new Teacher('Vladimir 4','HTML')

// console.log(teacher.name); // Vladimir 3
// teacher.sayHi()

// console.log(teacher2.name); // Vladimir 4

// console.log(Object.getPrototypeOf(teacher)); // Person
// console.log(Object.getPrototypeOf(teacher2)); // Person



// Object <- {}
// Object <- Array <- []
// Object <- String <- string
// Object <- Boolean <- boolean
// console.log(Object.getPrototypeOf());


// Object <- person
const person = {
	name: 'Vladimir',
	// toString() {
	// 	return 'this is object'
	// }
}
// alert(person) // [object Object] ( .toString() )
console.log(`${person}`) // [object Object] ( .toString() )































