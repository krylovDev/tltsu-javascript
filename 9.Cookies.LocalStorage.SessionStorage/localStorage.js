// console.log(localStorage)

localStorage.setItem('role', 'developer')
localStorage.setItem('theme', 'light')
localStorage.setItem('isActive', 'true')

// console.log(localStorage.getItem('theme') || 'no value') // value || null

localStorage.removeItem('isActive')

// localStorage.clear()

const arr = [11,22]
console.log(arr)
console.log(JSON.stringify(arr)) // data -> '...'
// console.log(typeof JSON.stringify(arr)) // string

console.log(JSON.parse('[11,22,33]')) // '...' -> data

async function getUsers() {
	const response = await fetch('https://jsonplaceholder.typicode.com/users') // object Response
	const usersList = await response.json() // []
	localStorage.setItem('usersList',JSON.stringify(usersList))
}

getUsers()

function renderUsers() {
	const usersList = JSON.parse(localStorage.getItem('usersList') || '[]')
	console.log(usersList)
	usersList.forEach((user) => {
		document.body.innerHTML += `
			<div>${user.name}</div>
			<div>${user.website}</div>
		`
	})
}

renderUsers()
